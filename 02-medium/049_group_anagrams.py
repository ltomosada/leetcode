# Given an array of strings strs, group the anagrams together. You can return
# the answer in any order.

# An Anagram is a word or phrase formed by rearranging the letters of a
# different word or phrase, typically using all the original letters
# exactly once.

# Example 1:

# Input: strs = ["eat","tea","tan","ate","nat","bat"]
# Output: [["bat"],["nat","tan"],["ate","eat","tea"]]
# Example 2:

# Input: strs = [""]
# Output: [[""]]
# Example 3:

# Input: strs = ["a"]
# Output: [["a"]]


# Constraints:

# 1 <= strs.length <= 104
# 0 <= strs[i].length <= 100
# strs[i] consists of lowercase English letters.
from collections import defaultdict


class Solution:
    def groupAnagrams(self, strs):
        anagrams = {}

        for str in strs:
            str_key = "".join(sorted(str))
            if str_key in anagrams:
                anagrams[str_key].append(str)
            else:
                anagrams[str_key] = [str]

        res = []
        for anagram in anagrams.values():
            res.append(anagram)

        return res

    def groupAnagrams2(self, strs):
        anagrams = defaultdict(list)

        for str in strs:
            count = [0] * 26

            for c in str:
                count[ord(c) - ord("a")] += 1
            anagrams[(tuple(count))].append(str)

        return anagrams.values()
